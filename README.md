# bylaws

General regulations to develop SIAM projects and code executions.

### Branching

1. Create a branch out of master
2. Name the branch with the following criteria: {branch-type}/{issue}-{branch-descriptive-name} being one of the posibles names `feature/SIAM-1-add-vessel-to-SIAM`
3. Commit your code in a descriptive way and review the Commits section to dig deeper into this.
4. Always push your code before leaving the office or turning off the computer.
5. Open a pull request and request one of your peers to review the code.
6. Never commit straight to master.

### Commits
1. The commit name must be descriptive.
2. It should be formatted as follows:
    a. {verb in past} {general overview of the changes performed}
    b. ej. `Fixed bug causing the frontpage to load forever`
3. Commit a lot and commit often.

### Testing
1. We require at least 85% of code coverage with unit testing.
2. At least 1 integration or aceptance test must be provided.

### QA
1. Never merge to master, always ask the QA engineer to handle this.
2. All the tests should be green before considering to merge.
3. Once merged is something else is needed please open a new ticket.

### Code splitting
1. If the code may be used in more than one place, please extract it to a package and create a new repository with a descriptive name.